#version 430 core

uniform vec4 lineColor;

out vec4 color;

void main()
{
    color = lineColor;
}
