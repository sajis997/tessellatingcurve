#version 430 core

//a pass-through vertex shader
//vertex position as attribute
in vec2  vPosition;

void main()
{
 
 gl_Position = vec4(vPosition,0.0,1.0);
}