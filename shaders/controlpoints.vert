#version 430 core

in vec2 vPosition;

uniform mat4 ModelviewProjection;

void main()
{
 gl_Position = ModelviewProjection * vec4(vPosition,0.0,1.0);
}