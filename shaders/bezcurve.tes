#version 430 core

//define the input primitive type
//it also indicates the type 
//of subdivision that is performed
//by the tesselation primitive generator
layout (isolines) in;

uniform mat4 ModelviewProjection;

void main()
{
   //gl_TessCoord contains the 
   //tesselation u and v coordinates
   //for this invocation. Since we are 
   //only tesselation in one dimension
   //we only need the u coordinate
   //that corresponds to the x-coordinate
   //gl_TessCoord	
   float u = gl_TessCoord.x;

   vec3 p0 = gl_in[0].gl_Position.xyz;
   vec3 p1 = gl_in[1].gl_Position.xyz;
   vec3 p2 = gl_in[2].gl_Position.xyz;
   vec3 p3 = gl_in[3].gl_Position.xyz;

   //now evaluate the bernstein polynomial
   float u1 = (1.0 - u);
   float u2 = u * u;

   //bernstein polynomial at u
   float b0 = u1 * u1 * u1;
   float b1 = 3.0 * u * u1 * u1;
   float b2 = 3.0 * u2 * u1;
   float b3 = u2 * u;

   //cubic bezier interpolation
   vec3 p = p0 * b0 + p1 * b1 + p2 * b2 + p3 * b3;

   gl_Position = ModelviewProjection * vec4(p, 1.0);
}