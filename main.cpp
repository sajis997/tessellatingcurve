#include "defines.h"
#include "GLSLShader.h"

//declare shader pointer to hold
//the shader for rendering curve
//and control points
GLSLShader *curveShader = NULL;
GLSLShader *controlPointShader = NULL;

GLFWwindow *window = NULL;

//hold the ID for the vertex array object
GLuint vaoID;

//hold the ID for the vertex buffer object
GLuint vboID;

glm::mat4 model = glm::mat4(1.0f);    // model-view matrix
glm::mat4 view = glm::mat4(1.0f);
glm::mat4 projection = glm::mat4(1.0f); // projection matrix

int winWidth = 512;
int winHeight = 512;

int numSegments = 3;

void startup();
void shutdown();
void loadShaders();
void render(float);

static void error_callback(int error, const char* description);
static void framebuffer_size_callback(GLFWwindow* window, int width, int height);
static void key_callback(GLFWwindow *window,int key,int scancode,int action,int mods);

int main()
{
    // Initialise GLFW before using any of its function
    if( !glfwInit() )
    {
       std::cerr << "Failed to initialize GLFW." << std::endl;
       exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    //we want the opengl 4
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 4);
    //we do not want the old opengl
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow( winWidth, winHeight, "Bezier Curve Demo", NULL, NULL);

    if( window == NULL )
    {
       fprintf( stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 4.3 compatible. Try the 2.1 version of the tutorials.\n" );

       //we could not initialize the glfw window
       //so we terminate the glfw
       glfwTerminate();
       exit(EXIT_FAILURE);
    }

    glfwSetErrorCallback(error_callback);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetKeyCallback(window,key_callback);

    //make the current window context current
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);

    glfwGetFramebufferSize(window,&winWidth,&winHeight);
    framebuffer_size_callback(window,winWidth,winHeight);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile

    if (glewInit() != GLEW_OK)
    {
       fprintf(stderr, "Failed to initialize GLEW\n");
       exit(EXIT_FAILURE);
    }

    if(!glewIsSupported("GL_VERSION_4_4"))
    {
       std::cerr << "OpenGL version 4.4 is yet to be supported. " << std::endl;
       exit(1);
    }

    while(glGetError() != GL_NO_ERROR) {}

    //print out information aout the graphics driver
    std::cout << std::endl;
    std::cout << "OpenGL Version: " << glGetString(GL_VERSION) << std::endl;
    std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GLEW Verion: " << glewGetString(GLEW_VERSION) << std::endl;
    std::cout << "OpenGL Vendor: " << glGetString(GL_VENDOR) << std::endl;
    std::cout << "Renderer: " << glGetString(GL_RENDERER) << std::endl;


    startup();

    do{
       render(static_cast<float>(glfwGetTime()));
       // Swap buffers
       glfwSwapBuffers(window);
       glfwPollEvents();

    } // Check if the ESC key was pressed or the window was closed
    while( glfwGetKey(window, GLFW_KEY_ESCAPE ) != GLFW_PRESS &&
       glfwWindowShouldClose(window) == 0 );


    //once the following function is called
    //no more events will be dilivered for that
    //window and its handle becomes invalid
    glfwDestroyWindow(window);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    shutdown();

    exit(EXIT_SUCCESS);
}


void startup()
{
    //define the number of initial segments
    numSegments = 3;


    loadShaders();

    glEnable(GL_DEPTH_TEST);

    float c = 3.5f;

    //define the orthographics projection
    projection = glm::ortho(-0.4f * c, 0.4f * c, -0.3f *c, 0.3f*c, 0.1f, 100.0f);

    glPointSize(10.0f);

    //setup the patch VBO
    float v[] = {-1.0f, -1.0f,
                 -0.5f, 1.0f,
                 0.5f, -1.0f,
                 1.0f, 1.0f};

    glGenBuffers(1,&vboID);
    glBindBuffer(GL_ARRAY_BUFFER,vboID);
    glBufferData(GL_ARRAY_BUFFER,8 * sizeof(float),v,GL_STATIC_DRAW);

    glGenVertexArrays(1,&vaoID);
    glBindVertexArray(vaoID);

    glVertexAttribPointer((*curveShader)["vPosition"],2,GL_FLOAT,GL_FALSE,0,BUFFER_OFFSET(0));
    glVertexAttribPointer((*controlPointShader)["vPosition"],2,GL_FLOAT,GL_FALSE,0,BUFFER_OFFSET(0));

    glEnableVertexAttribArray((*curveShader)["vPosition"]);
    glEnableVertexAttribArray((*controlPointShader)["vPosition"]);

    //now setup the number of vertices per patch
    glPatchParameteri(GL_PATCH_VERTICES,4);

    glm::vec4 lineColor = glm::vec4(1.0f,0.0f,0.5f,1.0f);

    curveShader->Use();

    glUniform1i((*curveShader)("numStrips"),1);
    glUniform4f((*curveShader)("lineColor"),lineColor.x,lineColor.y,lineColor.z,lineColor.w);

    curveShader->UnUse();

    glm::vec4 controlColor = glm::vec4(0.5f,1.0f,1.0f,1.0f);

    controlPointShader->Use();

    glUniform4f((*controlPointShader)("color"),controlColor.x,controlColor.y,controlColor.z,controlColor.w);

    controlPointShader->UnUse();
}

void shutdown()
{
    if(curveShader)
    {
        curveShader->DeleteShaderProgram();
        delete curveShader;
        curveShader = NULL;
    }

    if(controlPointShader)
    {
        controlPointShader->DeleteShaderProgram();
        delete controlPointShader;
        controlPointShader = NULL;
    }

    glDeleteBuffers(1,&vboID);
    glDeleteVertexArrays(1,&vaoID);
}

void loadShaders()
{
    //release any shader related resources if already allocated
    if(curveShader)
    {
        curveShader->DeleteShaderProgram();
        delete curveShader;
        curveShader = NULL;
    }

    if(controlPointShader)
    {
        controlPointShader->DeleteShaderProgram();
        delete controlPointShader;
        controlPointShader = NULL;
    }

    curveShader = new GLSLShader();
    curveShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/bezcurve.vert");
    curveShader->LoadFromFile(GL_TESS_CONTROL_SHADER,"shaders/bezcurve.tcs");
    curveShader->LoadFromFile(GL_TESS_EVALUATION_SHADER,"shaders/bezcurve.tes");
    curveShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/bezcurve.frag");

    curveShader->CreateAndLinkProgram();

    curveShader->Use();
    curveShader->AddAttribute("vPosition");

    curveShader->AddUniform("numSegments");
    curveShader->AddUniform("numStrips");
    curveShader->AddUniform("ModelviewProjection");
    curveShader->AddUniform("lineColor");
    curveShader->UnUse();

    controlPointShader = new GLSLShader();
    controlPointShader->LoadFromFile(GL_VERTEX_SHADER,"shaders/controlpoints.vert");
    controlPointShader->LoadFromFile(GL_FRAGMENT_SHADER,"shaders/controlpoints.frag");

    controlPointShader->CreateAndLinkProgram();

    controlPointShader->Use();
    controlPointShader->AddAttribute("vPosition");

    controlPointShader->AddUniform("color");
    controlPointShader->AddUniform("ModelviewProjection");

    controlPointShader->UnUse();
}

void render(float t)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //define the camera position
    glm::vec3 cameraPos(0.0f,0.0f,1.5f);

    //define the view matrix
    view = glm::lookAt(cameraPos,
                       glm::vec3(0.0f,0.0f,0.0f),
                       glm::vec3(0.0f,1.0f,0.0f));

    //calculate the model view matrix
    glm::mat4 modelview = view * model;



    curveShader->Use();
    glUniform1i((*curveShader)("numSegments"),numSegments);
    glUniformMatrix4fv((*curveShader)("ModelviewProjection"),1,GL_FALSE,glm::value_ptr(projection * modelview));

    //draw the curve
    glDrawArrays(GL_PATCHES,0,4);
    curveShader->UnUse();

    //draw the control points
    controlPointShader->Use();
    glUniformMatrix4fv((*controlPointShader)("ModelviewProjection"),1,GL_FALSE,glm::value_ptr(projection * modelview));
    glDrawArrays(GL_PATCHES,0,4);
    controlPointShader->UnUse();
}

void error_callback(int error, const char* description)
{
    fputs(description,stderr);
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    winWidth = width;
    winHeight = height;

    glViewport(0,0,winWidth,winHeight);

    //setup the projection matrix
    float c = 3.5f;

    //define the orthographics projection
    projection = glm::ortho(-0.4f * c, 0.4f * c, -0.3f *c, 0.3f*c, 0.1f, 100.0f);
}


void key_callback(GLFWwindow *window,int key,int scancode,int action,int mods)
{
    if(action != GLFW_PRESS) return;

    switch(key)
    {
        case GLFW_KEY_UP:
            numSegments++;
            break;
        case GLFW_KEY_DOWN:
            numSegments = numSegments > 1 ? numSegments - 1 : 1;
            break;
        case GLFW_KEY_R:
            //re-initialize the number of segments
            numSegments = 3;
            break;
        default:
            break;
    }
}
